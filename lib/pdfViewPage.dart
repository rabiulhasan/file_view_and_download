import 'package:file_view_and_download/api/ApiService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
class PdfViewPage extends StatefulWidget {
  const PdfViewPage({Key? key}) : super(key: key);

  @override
  State<PdfViewPage> createState() => _PdfViewPageState();
}

class _PdfViewPageState extends State<PdfViewPage> {
  String? localPath;
  String? url;
  ApiService api = ApiService();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    url = "https://www.ibm.com/downloads/cas/GJ5QVQ7X";
    api.loadPdfFile().then((value) {
      setState(() {
        localPath = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pdf file"),
        actions: [
          IconButton(
            onPressed: () async{
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text(
                    'successfully saved to internal storage "PDF_Download" folder',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            },
            icon: const Icon(Icons.download_rounded),
          ),
        ],
      ),
      body: localPath != null
          ? PDFView(
              filePath: localPath,
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
