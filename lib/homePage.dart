import 'dart:io';

import 'package:file_view_and_download/api/ApiService.dart';
import 'package:file_view_and_download/api/pdf_view_download.dart';
import 'package:file_view_and_download/pdfViewPage.dart';
import 'package:file_view_and_download/uploadFile.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ApiService api=ApiService();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PDF Viewer & Download'),
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return const UploadFilePage();
                  }));
                }, child: const Text('Upload File')),

            TextButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return const PdfViewPage();
                  }));
                }, child: const Text('Click for pdf viewer')),
            TextButton(
                onPressed: () async{
                  const url ="https://www.ibm.com/downloads/cas/GJ5QVQ7X";
                  final file = await api.loadPdfFromNetwork(url);
                  Navigator.push(context, MaterialPageRoute(builder: (context){
                    return  PdfViewerPage(file: file, url: url);
                  }));
                }, child: const Text('Click for pdf download'))
          ],
        ),
      ),
    );
  }

}
