import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class UploadFilePage extends StatefulWidget {
  const UploadFilePage({Key? key}) : super(key: key);

  @override
  State<UploadFilePage> createState() => _UploadFilePageState();
}

class _UploadFilePageState extends State<UploadFilePage> {
  TextEditingController fileUploadController=TextEditingController();
  //FirebaseFirestore? _firebaseFirestore;
  File file=File("");
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //_firebaseFirestore = FirebaseFirestore.instance;
  }
  void pickedFile() async{
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.any,
      //allowedExtensions: ['jpg', 'pdf', 'doc'],
    );
    if (result != null) {
      setState((){
        file=File(result!.files.single.path ?? "");
        //fileUploadController.text=file.path;
        fileUploadController.text=basename(file.path);
      });
      print(file.path);
    } else {
      // User canceled the picker
    }

  }
  void uploadFile() async{
    final fileName =basename(file.path) ;

    var url="https://fakestoreapi.com/products";
    var response = await http.post(Uri.parse(url), body: {
      "image": fileUploadController.text,
    });
    //var data = json.decode(response.body.toString());
    if(response.statusCode == 200){
      print('image uploaded');
    }else {
      print('failed');
    }
    // Reference firebaseStorageRef =
    // FirebaseStorage.instance.ref().child('uploads/$fileName');
    // UploadTask uploadTask = firebaseStorageRef.putFile(file);
    //
    // await uploadTask.whenComplete(() async {
    //   var url = await firebaseStorageRef.getDownloadURL();
    //   String file_url = url.toString();
    //   SavedFile(file_url);
    // }).catchError((onError) {
    //   print(onError);
    // });

  }
  // void SavedFile(String file_url) async {
  //   String status = await UploadDoctorImage(file_url);
  //   if (status == "upload") {
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('Success')));
  //     }
  //    else { ScaffoldMessenger.of(context)
  //     .showSnackBar(SnackBar(content: Text('Failed')));}
  // }
// Future<String> UploadDoctorImage(String file_url) async {
//   return await _firebaseFirestore
//       .collection("DoctorsInformation")
//       .add('file': file_url,
//   ).then((value){
//     return "success";
//   }).catchError((error){
//     return "Failed error: ${error}";
//   });
//
// }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Upload File'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: TextField(
              keyboardType: TextInputType.text,
              controller: fileUploadController,

              style: const TextStyle(fontSize: 20),
              decoration: InputDecoration(
                prefixIcon: const Icon(Icons.file_upload),
                hintText: 'File',
                filled: true,
                fillColor: Colors.grey.shade300,
                enabledBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey,
                  ),
                ),
                border: const OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blue),
                ),
              ),
            ),
          ),
          Padding(padding: EdgeInsets.all(10),
            child: TextButton(
              onPressed: (){
                pickedFile();
              },
              child: Text('Picked File'),
            ),
          ),
          Padding(padding: EdgeInsets.all(10),
            child: TextButton(
              onPressed: (){
                uploadFile();
              },
              child: Text('Upload File'),
            ),
          )
        ],
      ),
    );
  }
}
