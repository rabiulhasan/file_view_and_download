import 'dart:io';

import 'package:file_view_and_download/api/ApiService.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:path/path.dart';
class PdfViewerPage extends StatefulWidget {
  final File file;
  final String url;
  const PdfViewerPage({Key? key,required this.file,
    required this.url,}) : super(key: key);

  @override
  State<PdfViewerPage> createState() => _PdfViewerPageState();
}

class _PdfViewerPageState extends State<PdfViewerPage> {
  ApiService api=ApiService();
  String? localPath;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    localPath=widget.file.path;
  }
  @override
  Widget build(BuildContext context) {
    final name =basename(widget.file.path) ;
    return Scaffold(
      appBar: AppBar(
        title: Text(name),
        actions: [
          IconButton(
            onPressed: () async {
              await api.saveFile(widget.url, name);
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text(
                    'successfully saved to internal storage "Download" folder',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            },
            icon: const Icon(Icons.download_rounded),
          ),
        ],
      ),
      body:localPath!=null
          ? PDFView(
        filePath: localPath,
      ):const Center(
        child: CircularProgressIndicator(),
      ),
    );
  }


}
